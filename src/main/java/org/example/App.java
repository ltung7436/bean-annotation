package org.example;

import org.example.bean.HelloWorld;
import org.example.config.ConfigBean;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */

public class App
{
    public static void main( String[] args )
    {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConfigBean.class);

        HelloWorld helloWorld = (HelloWorld)context.getBean("helloWorldBean");
        System.out.println(helloWorld.getMessage());

        context.close();
    }
}
