package org.example.config;

import org.example.bean.HelloWorld;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigBean {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public HelloWorld helloWorldBean() {
        HelloWorld helloWorld = new HelloWorld();
        helloWorld.setMessage("Hello world!!! Java");
        return helloWorld;
    }


}
